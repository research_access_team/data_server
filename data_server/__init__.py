import os, logging
from logging import handlers

from flask import Flask, request
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from data_server.models import init_db
from data_server.resources.project_resource import (ProjectResource,
		ProjectsResource, YourProjectsResource)
from data_server.resources.research_data_file_resource import (
		ResearchDataFileResource)

from . import constants


def register_resources(app):
    api = Api(app)
    api.add_resource(ProjectResource, "/projects/<int:project_id>")
    api.add_resource(ProjectsResource, "/projects")
    api.add_resource(ResearchDataFileResource,
    	"/projects/<int:project_id>/<string:filename>")
    api.add_resource(YourProjectsResource, "/yourprojects")


def get_config_type():
    return os.environ.get(
    	constants.APP_CONFIG_ENV_VAR,
    	constants.PRODUCTION_CONFIG_VAR
	)


def config_app(app):
	config_type = get_config_type()

	# Possible configurations
	configs = {
		constants.DEVELOPMENT_CONFIG_VAR: "data_server.config.DevelopmentConfig",
		constants.PRODUCTION_CONFIG_VAR: "data_server.config.ProductionConfig"
	}

	app.config.from_object(configs[config_type])

	config_file_path = constants.APP_CONFIG_FILE_PATH

	if config_file_path and os.path.exists(config_file_path):
		app.config.from_pyfile(config_file_path)


def file_logging(app):
	app.logger.setLevel(logging.DEBUG)

	# Log formatter
	formatter = logging.Formatter(
		"[%(name)s](%(asctime)s) - %(levelname)s >>> %(message)s")

	# Add a stream handler (logs to terminal)
	stream_handler = logging.StreamHandler()
	stream_handler.setLevel(logging.DEBUG)
	stream_handler.setFormatter(formatter)
	app.logger.addHandler(stream_handler)

	# Add a rotating file handler (logs to a rotating file)
	rotating_file_handler = handlers.RotatingFileHandler(
		constants.LOGFILE_NAME,
		maxBytes=10000,
		backupCount=5,
		encoding='utf-8')
	rotating_file_handler.setLevel(logging.WARNING)
	rotating_file_handler.setFormatter(formatter)
	app.logger.addHandler(rotating_file_handler)


def sentry_logging(app):
	if not app.debug:
		app.sentry = sentry_sdk.init(
			dsn=constants.SENTRY_DSN,
			integrations=[FlaskIntegration()]
		)


def setup_logging(app):
	file_logging(app)
	sentry_logging(app)


def init_jwt(app):
	jwt = JWTManager(app)

	@jwt.token_in_blacklist_loader
	def check_if_token_in_blacklist(token):
		from data_server.models.blacklist import Blacklist
		jti = token["jti"]
		blacklist = Blacklist.query.filter_by(token=jti).first()
		return blacklist


def init_app(app):
	setup_logging(app)
	init_jwt(app)
	register_resources(app)
	init_db(app)

	# Enable CORS
	CORS(app, resources={r'/*': {'origins': '*'}})


def create_app(test_config=None):
	app = Flask(__name__)

	if test_config:
		app.config.from_mapping(test_config)
	else:
		config_app(app)
	
	init_app(app)

	return app
