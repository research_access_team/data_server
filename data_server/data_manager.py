from flask import current_app

import requests

from data_server.models import db
from data_server.models.project import Project

from data_server.injector_utils import get_dropbox_storage_provider


class DataManager(object):

	def __init__(self):
		self.dropbox_csp = get_dropbox_storage_provider()

	def upload_to_project_by_id(self, decrypted_file_stream, project,
		filename):

		# Set cursor to start of file
		decrypted_file_stream.seek(0)

		try:
			res = self.dropbox_csp.upload_stream(decrypted_file_stream, filename,
				str(project.id), "", True)

			if res is None:
				return False

			return True
		except requests.exceptions.ConnectionError:
			current_app.logger.error("Failed to connect to Dropbox, are you connected to the Internet?")
			return False

	def list_files(self, project_id):
		files_metadata = self.dropbox_csp.list_files(str(project_id), "")
		return files_metadata

	def fetch_file(self, project_id, filename):
		research_data_file_bytes = self.dropbox_csp.get_file(
			str(project_id), "", filename)
		return research_data_file_bytes

	def delete_file(self, project_id, filename):
		self.dropbox_csp.delete_file(str(project_id), "", filename)
