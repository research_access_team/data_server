from . import db


researchers = db.Table("researcher_project",
	db.Column("researcher_id", db.Integer,
		db.ForeignKey("researcher.id", primary_key=True)),
	db.Column("project_id", db.Integer,
		db.ForeignKey("project.id", primary_key=True))
)


class Project(db.Model):
	__tablename__  = "project"

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(300), nullable=False)

	researchers = db.relationship("Researcher", secondary=researchers, 
		backref="projects", lazy=True)

	policies = db.relationship("Policy", backref="project", cascade="delete", lazy=True)

	def __repr__(self):
		return "Project<title={}>".format(self.title)
