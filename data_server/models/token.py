from . import db


class Token(db.Model):
	__tablename__ = "data_server_token"

	id = db.Column(db.Integer, primary_key=True)
	jwt = db.Column(db.String(400), nullable=False, unique=True)

