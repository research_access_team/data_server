from . import db


class Policy(db.Model):
	__tablename__ = "policy"

	id = db.Column(db.Integer, primary_key=True)
	jsonified_policy = db.Column(db.String, nullable=False)

	project_id = db.Column(
		db.Integer, db.ForeignKey("project.id"), nullable=False)

	def __repr__(self):
		return "<Policy id={}>".format(
			self.id)
