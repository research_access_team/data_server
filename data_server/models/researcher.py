from . import db


class Researcher(db.Model):
	__tablename__ = "researcher"

	# Researcher info
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	email = db.Column(db.String(256), nullable=False, unique=True)

	# Department info
	department_id = db.Column(
		db.Integer, db.ForeignKey("department.id"), nullable=False)

	# projects = db.relationship("ResearcherProject", backref="researcher",
	# 	lazy=True)

	def __repr__(self):
		return "<Researcher name={}, department={}>".format(
			self.name, self.department)
