import click
from flask_sqlalchemy import SQLAlchemy
from flask.cli import with_appcontext


db = SQLAlchemy()


from .token import Token
from .college import College
from .school import School
from .department import Department
from .project import Project
from .researcher import Researcher
from .blacklist import Blacklist
from .policy import Policy


@click.command("create-db")
@with_appcontext
def create_db():
	db.create_all()
	click.echo("Created database")


@click.command("clear-db")
@with_appcontext
def clear_db():
	db.drop_all()
	click.echo("Cleared database")


def init_db(app):
	db.init_app(app)

	app.cli.add_command(create_db)
	app.cli.add_command(clear_db)
