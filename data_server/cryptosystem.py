import sys, os, requests, json, codecs, io

from flask import current_app
import pyAesCrypt

from data_server import constants

from data_server.models import db
from data_server.models.token import Token


sys.setrecursionlimit(5000)


def egcd(a, b):
	if a == 0:
		return (b, 0, 1)
	else:
		g, y, x = egcd(b % a, a)
		return (g, x - (b // a) * y, y)


def modinv(a, m):
	g, x, y = egcd(a, m)
	if g != 1:
		raise Exception('modular inverse does not exist')
	else:
		return x % m


def encrypt_using_rsa(message, recipient_public_key, public_params):
	n = public_params["n"]
	b = bytes(message, "UTF-8")
	message_int = int.from_bytes(b, byteorder="big")

	if recipient_public_key < 0:
		return pow(modinv(message_int, n), abs(recipient_public_key), n)

	return pow(message_int, recipient_public_key, n)


def decrypt_using_rsa(message, recipient_private_key, n):
	if recipient_private_key < 0:
		decrypted_message_int =  pow(
			modinv(message, n),
			abs(recipient_private_key),
			n)
	else:
		decrypted_message_int = pow(message, recipient_private_key, n)

	decrypted_message = decrypted_message_int.to_bytes(
		int(decrypted_message_int.bit_length() // 8) + 1, byteorder='big')

	return decrypted_message




def fetch_private_key(url, recipient_id):
		jwt = authenticate_with_pkg()

		response = requests.post(
			url,
			data=json.dumps({"id":recipient_id}),
			headers={
				"content-type":"application/json",
				"Authorization": "Bearer {}".format(jwt)
			}
		)

		if not (200 <= response.status_code < 300):
			current_app.logger.error(response.reason)
			raise ValueError("Private Key not obtained from PKG: {}, \
				check PKG state".format(
				response.reason))

		private_key = json.loads(response.text)["priv_key"]
		public_params = json.loads(response.text)["public_params"]

		return private_key, public_params


def authenticate_with_pkg():
	token = Token.query.first()

	if not token:
		url = constants.PKG_AUTH_URL
		password = os.getenv("DATA_SERVER_PW", "")

		response = requests.post(
			url,
			data=json.dumps({
				"email":"server@dserver.com",
				"password":password
			}),
			headers={"content-type":"application/json"}
		)

		if response.status_code != 200:
			current_app.logger.error("Unable to Authenticate with PKG.\
				\nCode: {}, Response: {}".format(
					response.status_code, response.text))
			raise("Unable to Authenticate")

		jwt = json.loads(response.text)["token"]

		try:
			token = Token()
			token.jwt = jwt

			db.session.add(token)
			db.session.commit()
		except Exception as e:
			current.logger.error(e)

	return token.jwt
