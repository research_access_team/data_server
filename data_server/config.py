import os


class BaseConfig(object):
	SQLALCHEMY_DATABASE_URI = os.getenv("RESEARCH_REPO_DB_URI", "")
	SECRET_KEY = os.getenv("PKG_SECRET_KEY", "SECRET_KEY")
	JWT_ALGORITHM = "HS256"
	JWT_BLACKLIST_ENABLED = True
	JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
	JWT_ACCESS_TOKEN_EXPIRES = 3600 * 24 * 7


class DevelopmentConfig(BaseConfig):
	SQLALCHEMY_TRACK_MODIFICATIONS = True
	ENV = "development"
	DEBUG = True


class ProductionConfig(BaseConfig):
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	ENV = "production"
	DEBUG = False
