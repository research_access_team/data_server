import os

from cloud_storage_provider.dropbox_csp.dropbox_cloud_storage_provider import (
	DropBoxStorageProvider)


def get_dropbox_storage_provider():
	dropbox_token = os.getenv("DROPBOX_TOKEN", None)
	assert dropbox_token is not None, "Dropbox Token not provided"

	return DropBoxStorageProvider(dropbox_token)