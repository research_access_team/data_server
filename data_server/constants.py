import os


PKG_SERVER_ADDRESS = os.getenv("PKG_SERVER_ADDRESS", "http://127.0.0.1:5000")
ALGO = os.getenv("ENC_ALGO", "rsa")

PKG_PRIVATE_KEY_URL = "{}/{}/{}".format(
	PKG_SERVER_ADDRESS,
	"private_key",
	ALGO)

PKG_PUBLIC_KEY_URL = "{}/{}/{}".format(
	PKG_SERVER_ADDRESS,
	"public_key",
	ALGO)

PKG_AUTH_URL = "{}/{}/{}".format(
	PKG_SERVER_ADDRESS,
	"auth",
	"login")

# Config
APP_CONFIG_ENV_VAR = "DATA_SERVER_CONFIG"
DEVELOPMENT_CONFIG_VAR = "dev"
PRODUCTION_CONFIG_VAR = "prod"

# Logging
LOGFILE_NAME = "data_server.log"
SENTRY_DSN = os.getenv("SENTRY_DSN", "")

# App
APP_CONFIG_FILE_PATH = os.getenv("APP_CONFIG_FILE_PATH", "")
