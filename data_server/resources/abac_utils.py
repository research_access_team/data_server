
def create_abac_policy(creators_policy, project_attr):
	print(project_attr)

	actions = creators_policy["actions"]
	resources = project_attr
	subjects = creators_policy["researcher_attr"]
	verdict = creators_policy["verdict"]

	from abac.abac_manager import ABACManager

	return ABACManager.create_jsonified_policy(actions, resources, subjects, verdict)


def create_abac_policy_from_json(jsonified_policy):
	from abac.abac_manager import ABACManager

	return ABACManager.create_policy_from_json(jsonified_policy)


def abac_allows(researcher, project, action):
	researcher_attr = {
		"college": researcher.department.school.college.id,
		"school": researcher.department.school.id,
		"department": researcher.department.id,
	}

	project_attr = {
		"college": project.researchers[0].department.school.college.id,
		"school": project.researchers[0].department.school.id,
		"department": project.researchers[0].department.id
	}

	policies = [create_abac_policy_from_json(policy.jsonified_policy) \
				for policy in project.policies]

	# return None
	from abac.abac_manager import ABACManager

	return ABACManager.check_authorized(project_attr, researcher_attr, policies, action)