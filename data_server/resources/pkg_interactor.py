import os, requests, json

from flask import current_app

from data_server.models import db
from data_server.models.token import Token
from data_server import constants


class PKGInteractor(object):

	@staticmethod
	def authenticate_with_pkg():
		token = Token.query.first()

		if not token:
			url = constants.PKG_AUTH_URL
			password = os.getenv("DATA_SERVER_PW", "")

			if not password:
				raise Exception("DATA SERVER - PKG Password Env Var not set")

			response = requests.post(
				url,
				data=json.dumps({
					"email":"server@dserver.com",
					"password":password
				}),
				headers={"content-type":"application/json"}
			)

			if response.status_code != 200:
				current_app.logger.error("Unable to Authenticate with PKG.\
					\nCode: {}, Response: {}".format(
						response.status_code, response.text))
				raise Exception("Unable to Authenticate")

			jwt = json.loads(response.text)["token"]

			try:
				token = Token()
				token.jwt = jwt

				db.session.add(token)
				db.session.commit()
			except Exception as e:
				current_app.logger.error(e)

		return token.jwt


class PKGPublicKeyInteractor(PKGInteractor):

	@staticmethod
	def fetch_public_key(url, recipient_id):
		jwt = PKGInteractor.authenticate_with_pkg()

		response = requests.post(
			url,
			data=json.dumps({"id":recipient_id}),
			headers={
				"content-type":"application/json",
				"Authorization": "Bearer {}".format(jwt)
			}
		)

		public_key = json.loads(response.text)["pub_key"]
		public_params = json.loads(response.text)["public_params"]

		return public_key, public_params


class PKGPrivateKeyInteractor(PKGInteractor):

	@staticmethod
	def fetch_private_key(url, recipient_id):
		jwt = PKGInteractor.authenticate_with_pkg()

		response = requests.post(
			url,
			data=json.dumps({"id":recipient_id}),
			headers={
				"content-type":"application/json",
				"Authorization": "Bearer {}".format(jwt)
			}
		)

		if not (200 <= response.status_code < 300):
			current_app.logger.error(response.reason)
			raise ValueError("Private Key not obtained from PKG: {}, \
				check PKG state".format(
				response.reason))

		private_key = json.loads(response.text)["priv_key"]
		public_params = json.loads(response.text)["public_params"]

		return private_key, public_params
