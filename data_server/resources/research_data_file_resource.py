import io, codecs, base64

from requests_toolbelt import MultipartEncoder

from flask import make_response, send_file, current_app
from flask_restful import reqparse, Resource
from flask_jwt_extended import jwt_required, get_raw_jwt

from .pkg_interactor import PKGPublicKeyInteractor
from ra_crypto.ibe.ibe_rsa import encrypt_using_rsa
from ra_crypto.aes.core import encrypt_file_using_aes_as_stream
from ra_crypto.aes.core import generate_aes_key

from data_server.data_manager import DataManager
from data_server.models.project import Project
from data_server.models.researcher import Researcher
from data_server import constants

from .abac_utils import abac_allows


class ResearchDataFileResource(Resource, PKGPublicKeyInteractor):

	@jwt_required
	def get(self, project_id, filename):
		project = Project.query.get(project_id)

		email = get_raw_jwt()["identity"]
		researcher = Researcher.query.filter_by(email=email).first()

		if not project:
			return {}, 404

		if not abac_allows(researcher, project, "download") and email != project.researchers[0].email:
			return {}, 403

		researcher_id = get_raw_jwt()["identity"]

		# Fetch research data file
		file_bytes = ResearchDataFileResource.fetch_file(project_id, filename)

		if not file_bytes:
			return {}, 500

		# Encrypt the research data file
		(encrypted_research_data_file, 
			encrypted_aes_key) = ResearchDataFileResource.get_encrypted_file_stream(
			file_bytes, researcher_id)

		# Send / Return encrypted research data file and aes key
		m = MultipartEncoder(
			fields={'key': str(encrypted_aes_key),
					'file': (
						filename,
						encrypted_research_data_file
					)
			}
		)

		return make_response(m.to_string(), {"Content-Type": m.content_type})

	@jwt_required
	def delete(self, project_id, filename):
		try:
			data_manager = DataManager()
			data_manager.delete_file(project_id, filename)
			return {}, 200
		except Exception as exc:
			current_app.logger.error(exc)
			return {}, 500

	@staticmethod
	def fetch_file(project_id, filename):
		data_manager = DataManager()
		file_bytes = data_manager.fetch_file(project_id, filename)
		return file_bytes


	@staticmethod
	def get_encrypted_file_stream(plain_text_file_bytes, recipient_id):
		public_key, public_param = ResearchDataFileResource.fetch_public_key(
			constants.PKG_PUBLIC_KEY_URL,
			recipient_id)

		# Generate an AES Key
		aes_key = generate_aes_key()

		# Encrypt research data file
		encrypted_research_data_file = encrypt_file_using_aes_as_stream(
			plain_text_file_bytes, aes_key)

		# Encrypt the AES Key
		encrypted_aes_key = encrypt_using_rsa(aes_key, int(public_key),
			int(public_param["rsa"]["n"]))

		# return encrypted_research_data_file, encrypted_aes_key
		return encrypted_research_data_file, encrypted_aes_key
