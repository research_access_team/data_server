import io, requests, json, codecs

from flask import g, current_app, make_response, request
from flask_restful import reqparse, Resource
from flask_jwt_extended import jwt_required, get_raw_jwt

from ra_crypto.ibe.ibe_rsa import decrypt_using_rsa
from ra_crypto.aes.core import decrypt_file_using_aes_as_stream

from .pkg_interactor import PKGPrivateKeyInteractor
from data_server.data_manager import DataManager
from data_server.models import db
from data_server.models.researcher import Researcher
from data_server.models.project import Project
from data_server.models.token import Token
from data_server.models.policy import Policy
from data_server.utils import non_empty_string
from data_server import constants

from .abac_utils import *


class ProjectsResource(Resource):

	@jwt_required
	def get(self):	
		email = get_raw_jwt()["identity"]
		researcher = Researcher.query.filter_by(email=email).first()

		projects = Project.query.all()

		return {"projects": [
					{"id": project.id, 
					"title": project.title, 
					"owner": project.researchers[0].name,
					"college": project.researchers[0].department.school.college.name,
					"school": project.researchers[0].department.school.name,
					"department": project.researchers[0].department.name} \
					for project in projects if abac_allows(researcher, project, "view")]
				}

	@jwt_required
	def post(self):
		root_args = request.get_json()

		title = root_args["title"]
		policies = root_args["policies"]

		email = get_raw_jwt()["identity"]
		researcher = Researcher.query.filter_by(email=email).first()

		try:
			project = Project()
			project.title = title
			project.researchers.append(researcher)

			ProjectsResource.add_policies(policies, project)

			db.session.add(project)
			db.session.commit()
		except Exception as e:
			current_app.logger.error(e)
			return {}, 500

		return {}, 201

	@staticmethod
	def add_policies(policies, project):
		for creators_policy in policies:
			jsonified_policy = create_abac_policy(creators_policy,
				{"department": project.researchers[0].department.id})

			policy = Policy()
			policy.jsonified_policy = jsonified_policy

			project.policies.append(policy)

			db.session.add(policy)


class ProjectResource(Resource, PKGPrivateKeyInteractor):
	id = "server@dserver.com"

	@jwt_required
	def get(self, project_id):
		data_manager = DataManager()
		files_metadata = data_manager.list_files(project_id)

		return {"files": [{
			"name": file_metadata.name,
			"format": file_metadata.name.split(".")[-1],
			"size": file_metadata.size
			} for file_metadata in files_metadata]
		}

	@jwt_required
	def post(self, project_id):
		# Handle request arguments
		parser = reqparse.RequestParser()
		parser.add_argument("files", required=True, nullable=False,
			type=non_empty_string, location="files")
		parser.add_argument("key",  required=True, nullable=False,
			type=int, location="form")
		args = parser.parse_args()

		encrypted_file = args["files"]
		encrypted_aes_key = args["key"]

		encrypted_file_bytes = encrypted_file.read()
		
		# Obtain decrypted file
		decrypted_file_stream = ProjectResource.get_decrypted_file_stream(
			encrypted_file_bytes, encrypted_aes_key)

		# Upload the file to cloud storage under specified project
		project = Project.query.get(project_id)
		if not project:
			return {}, 404

		data_manager = DataManager()
		res = data_manager.upload_to_project_by_id(io.BytesIO(decrypted_file_stream.read()),
			project, encrypted_file.filename)

		if not res:
			current_app.logger.error("Unable to upload file")
			return {}, 500

		return {}, 200

	@staticmethod
	@jwt_required
	def put(project_id):
		researcher_email = get_raw_jwt()["identity"]
		researcher = Researcher.query.filter_by(email=researcher_email).first()

		project = Project.query.get(project_id)
		if not project:
			return {}, 404

		if not researcher_email == project.researchers[0].email:
			return {}, 401

		root_args = request.get_json()

		title = root_args["title"]
		policies = root_args["policies"]


		try:
			project.title = title
			ProjectResource.update_policies(policies, project)

			db.session.commit()
		except Exception as e:
			current_app.logger.error(e)
			return {}, 500
		return {}, 200

	@staticmethod
	@jwt_required
	def delete(project_id):
		# Confirm user identity
		researcher_email = get_raw_jwt()["identity"]

		project = Project.query.get(project_id)
		if not project:
			return {}, 404

		if not researcher_email == project.researchers[0].email:
			return {}, 401

		try:
			db.session.delete(project)
			db.session.commit()
		except Exception as e:
			current_app.logger.error(e)
			return {}, 500
		return {}, 200

	@staticmethod
	def update_policies(policies, project):
		# Delete existing policy
		old_policies = Policy.query.filter_by(project_id=project.id).all()
		for old_policy in old_policies:
			db.session.delete(old_policy)

		for creators_policy in policies:
			jsonified_policy = create_abac_policy(creators_policy,
				{"department": project.researchers[0].department.id})

			policy = Policy()
			policy.jsonified_policy = jsonified_policy

			project.policies.append(policy)

			db.session.add(policy)

	@staticmethod
	def get_decrypted_file_stream(encrypted_file_bytes, encrypted_aes_key):
		# Make request to PKG for private key
		private_key, public_param = ProjectResource.fetch_private_key(
			constants.PKG_PRIVATE_KEY_URL,
			ProjectResource.id)

		# Decrypt AES Key using IBE
		aes_key_hex = decrypt_using_rsa(encrypted_aes_key,
			int(private_key), int(public_param["rsa"]["n"]))

		aes_key = codecs.decode(aes_key_hex, "hex")

		# Use AES Key to decrypt file
		decrypted_file_bytes = decrypt_file_using_aes_as_stream(
			encrypted_file_bytes, aes_key)
		
		def byte_array_string_to_byte_array(byte_array_string):
			return bytes([int(x) for x in str(byte_array_string)[2:-1].split(",")])

		decrypted_file_bytes = byte_array_string_to_byte_array(decrypted_file_bytes)

		return io.BytesIO(decrypted_file_bytes)

class YourProjectsResource(Resource):

	@staticmethod
	@jwt_required
	def get():
		researcher_email = get_raw_jwt()["identity"]
		researcher = Researcher.query.filter(Researcher.email==researcher_email).first()

		projects = researcher.projects

		return {"projects": [
					{"id": project.id, 
					"title": project.title, 
					"owner": project.researchers[0].name,
					"college": project.researchers[0].department.school.college.name,
					"school": project.researchers[0].department.school.name,
					"actions": [action["val"] for action in json.loads(
						project.policies[0].jsonified_policy)["actions"]],
					"department": project.researchers[0].department.name} \
					for project in projects]
				}
