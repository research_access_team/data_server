def non_empty_string(s: str):
    if not s:
        raise ValueError("Expected a non empty string")
    return s
