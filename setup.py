from setuptools import setup, find_packages


requirements_file = open("requirements.txt")
# Remove newline characters in the list of requirements
requirements = [requirement.strip() for requirement in \
				requirements_file.readlines()]

setup(
	name="data_server",
	version="1.0.0",
	description="Data server for research data files",
	long_description=open("README.md", "r").read(),
	packages=find_packages(),
	include_package_data=True,
	author="Eddy Mwenda",
	install_requires=requirements,
)
