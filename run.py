import os

from data_server import create_app


# Default to port 5000
port = os.getenv("PORT", 5000)


if __name__ == "__main__":
	app = create_app()
	app.run(host="0.0.0.0", port=port)
