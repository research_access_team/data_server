import json, requests

import pytest
from flask.testing import FlaskClient

from data_server import create_app
from data_server.models import create_db, clear_db


@pytest.fixture
def app():
	test_config = dict(
        TESTING=True,
        JSONIFY_PRETTYPRINT_REGULAR=False,
        SQLALCHEMY_DATABASE_URI="sqlite:////tmp/test.db",
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SECRET_KEY="test_secret_key",
        JWT_ALGORITHM="HS256",
        JWT_BLACKLIST_ENABLED=True,
        JWT_BLACKLIST_TOKEN_CHECKS=["access", "refresh"]
    )

	app = create_app(test_config)

	runner = app.test_cli_runner()
	
	runner.invoke(create_db)
	yield app
	runner.invoke(clear_db)


@pytest.fixture
def client(app):
	yield app.test_client()
